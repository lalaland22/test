import { IsNotEmpty, IsNumber } from "class-validator";
import { Client } from "src/Models/client.model";


export class QuoteDTO {
    
    @IsNotEmpty()
    client: Client;
        
    @IsNumber()
    @IsNotEmpty()
    totalAmount: number;
    
}
