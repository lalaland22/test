import { IsEmail, IsNotEmpty, IsNumber, IsString } from "class-validator";


export class ClientDTO {

@IsNotEmpty()
@IsString()
name: string;

@IsNotEmpty()
@IsString()
address: string;

@IsString()
@IsNotEmpty()
@IsEmail()
email: string;

@IsNumber()
@IsNotEmpty()
phoneNo: number;

}
