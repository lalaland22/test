import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class ProductDTO  {

@IsNotEmpty()
@IsString()
name: string;

@IsNotEmpty()
@IsNumber()
basePrice: string;

@IsNotEmpty()
@IsNumber()
value: number;

subProducts: [{
    name : string,
    price : number
    }]

}
