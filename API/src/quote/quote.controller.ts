import { Body, Controller, Delete, Get, Param, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { QuoteDTO } from 'src/DTOs/quote.DTO';
import { Quote } from 'src/Models/quote.model';
import { QuoteService } from './quote.service';

@Controller()
@UsePipes(new ValidationPipe())
export class QuoteController {
    constructor(private quoteservice : QuoteService){}
    @Post('createQuote')
    createquote(@Body() quote : QuoteDTO ){
        return this.quoteservice.createquote(quote);
    }

    @Get('getQuote/:id')
    getquote(@Param('id')id){
        return this.quoteservice.getquote(id);
    }
    
    @Get('getQuotes')
    getquotes(){
        return this.quoteservice.getquotes();
    }
    
    @Post('updateQuote/:id')
    updatequotes(@Param('id')id, @Body()update: Partial<Quote>){
        return this.quoteservice.updatequote(id, update);
    }
    
    @Delete('deleteQuote/:id')
    deletequotes(@Param('id')id){
        return this.quoteservice.deletequote(id);
    }
}
