import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { QuoteDTO } from 'src/DTOs/quote.DTO';
import { Quote } from 'src/Models/quote.model';

@Injectable()
export class QuoteService {
    constructor(@InjectModel (Quote.name) private quotemodel : Model <Quote>){}
        
    async createquote(quote: Quote | QuoteDTO) {
            let no : number;
            let month = new Date().getMonth() + 1 ;

            const newquote = new this.quotemodel(quote);            
            const quotes = await this.quotemodel.find({monthgenerated : month});
            if(quotes.length > 0){
            let subStringArray = quotes.map(
                (element) => {
                    return Number.parseInt(element.quoteNo.substring(18));
                }
            );
            let outputsubStringArray = subStringArray.sort(
                (a,b) => a-b
            );
            no = (outputsubStringArray.slice((outputsubStringArray.length - 1), outputsubStringArray.length))[0] + 1;
            }
            else {
                no = 1;
            }

            newquote.quoteNo = `SIBASI/${new Date().getFullYear()}/QUO/${new Date().getMonth() + 1}/${no}`;
            newquote.timegenerated = Date.now();
            newquote.quotenumberbyperiod = no; 
            return await newquote.save();
           
      }
         
    async getquote(id) {
          const quote = await this.quotemodel.findById(id);
          return quote;
     }
    
     async getquotes() {
        const quotes = await this.quotemodel.find();
        return quotes;
    }
    
     async updatequote(id, update) {
        const quote = await this.quotemodel.findByIdAndUpdate(id, update);
        await quote.save();
        const updatedquote = await this.quotemodel.findById(id);
        return updatedquote;
    }
    
    async deletequote(id){
        await this.quotemodel.findByIdAndDelete(id);
        return await this.quotemodel.find();
    }

}