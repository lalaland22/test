import { Module } from '@nestjs/common';
import { QuoteService } from './quote.service';
import { QuoteController } from './quote.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Quote, quotesSchema } from 'src/Models/quote.model';

@Module({
  imports : [MongooseModule.forFeature([{name: Quote.name, schema: quotesSchema}])],
  providers: [QuoteService],
  controllers: [QuoteController]
})
export class QuoteModule {}
