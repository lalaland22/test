import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import {Document} from 'mongoose';

@Schema()
export class Client extends Document {

@Prop({required: true})
name: string;

@Prop({required : false})
clientId: string;

@Prop({required: true})
address: string;

@Prop({required: true})
email: string;

@Prop({required: true})
phoneNo: number;

@Prop()
location: string;

@Prop()
contactPerson: string;

@Prop()
contactPersonNo: number;

@Prop()
contactPersonEmail: string;

}

export const clientSchema = SchemaFactory.createForClass(Client);

