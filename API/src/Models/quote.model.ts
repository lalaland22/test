import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import {Document} from 'mongoose';
import { Client } from "./client.model";
import { QuoteProductModel } from "./product.model";


@Schema()
export class Quote extends Document {

@Prop({required : false})
quoteNo: string;

@Prop({required: true})
client: Client;

@Prop()
attentionOf: string;

@Prop()
products: QuoteProductModel[];

@Prop({required: true})
totalAmount: number;

@Prop({default: false})
VAT: boolean;

@Prop({required: false, default: 'valid'})
status: string;

@Prop({required : false})
timegenerated : number;

@Prop({required : false, default: new Date().toLocaleDateString()})
dategenerated: string;

@Prop({required : false, default: new Date().getMonth() + 1})
monthgenerated: number;

@Prop({required : false})
quotenumberbyperiod : number;

}

export const quotesSchema = SchemaFactory.createForClass(Quote);