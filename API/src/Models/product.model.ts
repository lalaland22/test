import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import {Document} from 'mongoose';


@Schema()
export class Product extends Document {

@Prop({required: true})
name: string;

@Prop({required: true})
basePrice: number;

@Prop({required: true})
value: number;

@Prop()
quantity: number;

@Prop()
subProducts: [{
    name : string,
    price : number
    }]

}

export const productSchema = SchemaFactory.createForClass(Product);

export class QuoteProductModel{
    
    name: string;

    quantity: number;

    total: number;

    price: number;

    subProducts: [{
        name : string,
        quantity : number,
        price: number;
        }];

}
