import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ClientDTO } from 'src/DTOs/client.DTO';
import { Client } from 'src/Models/client.model';

@Injectable()
export class ClientService {
    constructor(@InjectModel(Client.name) private clientmodel : Model <Client>){}

    async createclient(client: Client | ClientDTO){
        const newclient = await new this.clientmodel(client);
        newclient.clientId = `${newclient.name.substring(0, 3) + new Date().getFullYear()}`;
        return await newclient.save();
    }

    async getclient(id ) {
        const client = await this.clientmodel.findById(id);
        return client;
    }
         
   async getclients() {
      const clients = await this.clientmodel.find();
      return clients;
  }
  
   async updateclient(id, update) {
      const client = await this.clientmodel.findByIdAndUpdate(id, update);
      client.save();
      const clients = await this.clientmodel.find();
      return clients;
  }
  
  async deleteclient(id){
      await this.clientmodel.findByIdAndDelete(id);
      const clients = await this.clientmodel.find();
      return clients;
  }
}
