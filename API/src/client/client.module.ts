import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Client, clientSchema } from 'src/Models/client.model';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';

@Module({
  imports : [MongooseModule.forFeature([{name : Client.name, schema : clientSchema}])],
  controllers: [ClientController],
  providers: [ClientService]
})
export class ClientModule {}
