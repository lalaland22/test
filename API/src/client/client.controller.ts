import { Body, Controller, Delete, Get, Param, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { Client } from 'src/Models/client.model';
import { ClientDTO } from 'src/DTOs/client.DTO';
import { ClientService } from './client.service';

@Controller()
@UsePipes(new ValidationPipe())
export class ClientController {
    constructor(private clientservice : ClientService){}

@Post('createClient')
createClient(@Body()client : ClientDTO){
    return this.clientservice.createclient(client);
}

@Get('getClient/:id')
getClient(@Param('id')id){
    return this.clientservice.getclient(id);
}

@Get('getClients')
getClients(){
    return this.clientservice.getclients();
}

@Post('updateClient/:id')
updateClients(@Param('id')id, @Body()update: Partial<Client>){
    return this.clientservice.updateclient(id, update);
}

@Delete('deleteClient/:id')
deleteClients(@Param('id')id){
    return this.clientservice.deleteclient(id);
}

}
