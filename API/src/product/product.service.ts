import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product } from 'src/Models/product.model';

@Injectable()
export class ProductService {
    constructor(@InjectModel (Product.name) private productmodel : Model <Product>){}
        
async createproduct(product ) {
      const newproduct = new this.productmodel(product);
      return await newproduct.save();
  }
     
async getproduct(id) {
      const product = await this.productmodel.findById(id);
      return product;
 }

 async getproducts() {
    const products = await this.productmodel.find();
    return products;
}

 async updateproduct(id, update) {
    const product = await this.productmodel.findByIdAndUpdate(id, update);
    product.save();
    const products = await this.productmodel.find();
    return products;
}

async deleteproduct(id){
    await this.productmodel.findByIdAndDelete(id);
    const products = await this.productmodel.find();
    return products;
}
}
