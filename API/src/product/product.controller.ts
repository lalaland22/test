import { Body, Controller, Delete, Get, Param, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { ProductDTO } from 'src/DTOs/product.DTO';
import { Product } from 'src/Models/product.model';
import { ProductService } from './product.service';

@Controller()
@UsePipes(new ValidationPipe())
export class ProductController {
    constructor(private productservice : ProductService){}

    @Post('createProduct')
    createProduct(@Body() product: ProductDTO){
        return this.productservice.createproduct(product);
    }

    @Get('getProduct/:id')
    getproduct(@Param('id')id){
        return this.productservice.getproduct(id);
    }
    
    @Get('getProducts')
    getproducts(){
        return this.productservice.getproducts();
    }
    
    @Post('updateProduct/:id')
    updateproducts(@Param('id')id, @Body()update: Partial<Product>){
        return this.productservice.updateproduct(id, update);
    }
    
    @Delete('deleteProduct/:id')
    deleteproducts(@Param('id')id){
        return this.productservice.deleteproduct(id);
    }
}
