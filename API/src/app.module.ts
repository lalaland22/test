import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientModule } from './client/client.module';
import { ProductModule } from './product/product.module';
import { QuoteModule } from './quote/quote.module';

export const mongoURL = 'mongodb://localhost:27017/Quotation';

@Module({
  imports: [
    ClientModule,
    MongooseModule.forRoot(mongoURL),
    ProductModule,
    QuoteModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
